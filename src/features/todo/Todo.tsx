import React, { useState } from 'react';

import { useAppSelector, useAppDispatch } from '../../app/hooks';
import {
  addTodo,
  deleteTodo,
  checkTodo,
  selectTodos,
  todo,
} from './todoSlice';
import styles from './Todo.module.css';

export function Todos() {
  const todos = useAppSelector(selectTodos);
  const dispatch = useAppDispatch();
  const [name, setName] = useState('');

  const submitTodo = () => {

    const id = String(Math.floor(Math.random() * 2000000))
    dispatch(addTodo({id:id, name,completed: false}))
    setName(' ')

  }

  return (
    <div>
      <div className={styles.row}>

        <input type="text" name="todo" value={name}  onChange={ (e)=> setName(e.target.value)} placeholder="add a task"/>
        </div>
        <button onClick={ submitTodo } > agregar</button>

        <div  className={styles.column}>

       
        {
          todos.map((todo, index) => (
            <div key={ index }>
              <p> {todo.id}  - {todo.name} - 
               <label>
               <input type="checkbox" defaultChecked={todo.completed ? true : false} onClick={ ()=> dispatch(checkTodo(todo.id)) }/>
               {todo.completed ? "completada" : 'Incompleta'}
               </label>
              </p>
              <button onClick={ ()=> dispatch(deleteTodo(todo.id)) }> Delete</button>
            </div>
          ))
        }
 </div>
       
    
    </div>
  );
}
