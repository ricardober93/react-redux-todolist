import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState, AppThunk } from '../../app/store';
import { fetchCount } from './todoAPI';


export interface todo {
  id: string;
  name: string;
  completed: boolean;
}

export interface TodoState {
  list: todo[];
}

const initialState: TodoState = {
  list: [],
};

// The function below is called a thunk and allows us to perform async logic. It
// can be dispatched like a regular action: `dispatch(incrementAsync(10))`. This
// will call the thunk with the `dispatch` function as the first argument. Async
// code can then be executed and other actions can be dispatched. Thunks are
// typically used to make async requests.

export const todoSlice = createSlice({
  name: 'todo',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    addTodo: (state,  action: PayloadAction<todo>) => {
      state.list = [ ...state.list, action.payload];
    },
    deleteTodo: (state,  action: PayloadAction<string>) => {
      state.list = state.list.filter( l => l.id !== action.payload)
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    checkTodo: (state, action: PayloadAction<string>) => {
      const todo = state.list.find( l =>  l.id === action.payload )
      todo!.completed = todo?.completed === false ? true : false
    },
  },
  // The `extraReducers` field lets the slice handle actions defined elsewhere,
  // including actions generated by createAsyncThunk or in other slices.

});

export const { addTodo, deleteTodo, checkTodo } = todoSlice.actions;

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state: RootState) => state.counter.value)`
export const selectTodos = (state: RootState) => state.todo.list

// We can also write thunks by hand, which may contain both sync and async logic.
// Here's an example of conditionally dispatching actions based on current state.


export default todoSlice.reducer;
